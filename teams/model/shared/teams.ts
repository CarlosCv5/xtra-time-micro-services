export class TeamProfile {
    team_id: string
    name: string
    sport: string
    users_id: [string]
    admins: [string]

    constructor(data: any) {
        this.team_id = data.team_id;
        this.name = data.name;
        this.sport = data.sport;
        this.users_id = data.users_id;
        this.admins = data.admins;
    }
}