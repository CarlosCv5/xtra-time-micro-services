export class UserProfile {
    user_id: Number
    name?: string
    sport?: string
    teams?: [Number]
    constructor(data: any) {
        this.user_id = data.user_id
        this.name = data.name;
        this.sport = data.sport;
        this.teams = data.teams;
    }
}