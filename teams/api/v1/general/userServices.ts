import request from "request";
import { response } from "express";
import { PublicInfo } from "../../../model/shared/messages";

export class UserServices {
    addTeamToUser(userID: String, teamID: String, cb: (statusCode: any) => any) {
        request.patch('http://users-srv:8093/v1/users/' + userID +'/team/' +teamID, (error: any, response: any, body: any) => {
            console.log('body', body, '     body.status ',body[0].status);
            const resp=JSON.parse(body);
            let message = new PublicInfo(resp.message,resp.status);
            
            cb(message.status);
        });
    }
    rmvTeamFromUser(teamID: String, userID: String, cb: (statusCode: number) => any) {
        request.delete('http://users-srv:8093/v1/users/' + userID + '/team/' + teamID, (error: any, response: any, body: any) => {
            const resp = JSON.parse(body);
            let message = new PublicInfo(resp.message, resp.status);
            cb(message.status);
        });
    }
}