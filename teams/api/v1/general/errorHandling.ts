import { RequestHandler, ErrorRequestHandler } from "express";

export const apiErrorHandler: ErrorRequestHandler = (err, req, res, next) => {
     switch(req.app.get("env")) {
         case "development":
            console.log(err);
            const error={status:err.status,properties:err.properties};
            return res.json(error);
        case "production":
            // production logging
            return res.status(err.status).json(err.publicVersion());
     }
};