import { Router } from "express";
import { teamsRoute } from "./teams/apiTeams";
import { logger } from "./general/logging";
import { apiErrorHandler } from "./general/errorHandling";

export let routerV1 = Router();

routerV1.use(logger);
routerV1.use("/teams", teamsRoute);
routerV1.use(apiErrorHandler);