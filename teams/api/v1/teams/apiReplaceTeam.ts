import { RequestHandler } from "express";

import { Team } from "../../../db/team";
import { APIError, PublicInfo } from "../../../model/shared/messages";


export const apiReplaceTeam: RequestHandler = (req, res, next) => {
    const teamID = req.params.id;
    const replaceTeam = req.body
    console.log(replaceTeam);
    console.log(teamID);
    const findTeam = Team.replaceOne({ team_id: teamID }, replaceTeam).then(data => {
        console.log(data);
        if (!data) {
            return next(APIError.errNotFound('Team not Found!'));
        }
        else {
            return next(PublicInfo.infoUpdated());
        }
    })
}