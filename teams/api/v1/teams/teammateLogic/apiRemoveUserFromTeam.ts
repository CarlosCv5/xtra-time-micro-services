import { RequestHandler } from "express";
import { Team } from "../../../../db/team";
import { APIError, PublicInfo } from "../../../../model/shared/messages";
import { BodyChecker } from "../../../../model/shared/body";
import { UserServices } from "../../general/userServices";

export const apiRemoveUserTeam: RequestHandler = async (req, res, next) => {
    const teamID = req.params.id;
    const userRemove = req.params.user;

    Team.findOneAndUpdate({ team_id: teamID, users_id: userRemove }, { $pull: { users_id: userRemove } }).exec(async function (err, doc) {
        if(!doc){
            return next (APIError.errNotFound());
        }
        return next(PublicInfo.infoDeleted());

    });
}



