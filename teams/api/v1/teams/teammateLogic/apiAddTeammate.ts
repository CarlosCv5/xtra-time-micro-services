import { RequestHandler } from "express";
import { UserServices } from "../../general/userServices";
import { Team } from "../../../../db/team";
import { APIError, PublicInfo } from "../../../../model/shared/messages";
 
export const apiAddTeammate: RequestHandler = (req, res, next) => {
    const userID = req.params.user;
    const teamID = req.params.id;
    console.log('User Id ',userID);
    console.log('Team ID ', teamID);
    Team.findOneAndUpdate({ team_id: teamID },{$push:{users_id:userID}}).exec(async function (err, doc) {
        if(!doc){
            return next(APIError.errNotFound());
        }
        return next(PublicInfo.infoUpdated());

    });
}
