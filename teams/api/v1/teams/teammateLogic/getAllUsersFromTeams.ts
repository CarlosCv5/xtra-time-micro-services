import { RequestHandler } from "express";

import { Team } from "../../../../db/team";
import { TeamProfile } from "../../../../model/shared/teams";
import { APIError } from "../../../../model/shared/messages";

export const apiGetAllUsersFromTeam: RequestHandler = (req, res, next) => {
    const teamID=req.params.id;
    Team.findOne({team_id:teamID}, 'users_id').then(data => {
        if (!data){
            return next (APIError.errNotFound());
        }

        return res.json(data.get('users_id'));
    });
}