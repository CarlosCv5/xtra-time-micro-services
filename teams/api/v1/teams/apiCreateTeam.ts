import { RequestHandler } from "express";
import { Team } from "../../../db/team";
import {v4 as uuidv4} from "uuid";
import { BodyChecker } from "../../../model/shared/body";
import { APIError } from "../../../model/shared/messages";

export const apiCreateTeam: RequestHandler = (req, res, next) => {
    const requiredFields=["name"];
    if(!new BodyChecker(requiredFields,req.body).checkBodyFields()){
        return next(APIError.errMissingBody("Body Inexistent"));
    }
    const newTeam = {
        team_id: uuidv4() || "",
        name: req.body.name || "",
        sport: req.body.sport || "",
        users_id: req.body.users_id,
        admins: req.body.admins,
    }
    new Team(newTeam).save(() => {
        res.json("New Team created ");
    });
};