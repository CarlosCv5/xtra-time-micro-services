import { RequestHandler } from "express";
import { TeamFilters } from '../../../model/shared/teamFilters';
import { APIError } from "../../../model/shared/messages";

export const checkTeamFilters: RequestHandler = (req, res, next) => {
    const filters = new TeamFilters(req.query)
    //Checks if invalid filters were provided
    for (let filter of Object.getOwnPropertyNames(req.query)) {
        if (!filters.hasOwnProperty(filter)) {
            return next(APIError.errInvalidQueryParameter("Invalid Query Parameters"));
        }
    }
    next();
};