import { RequestHandler } from "express";

import { Team } from "../../../db/team";
import { PublicInfo, APIError } from "../../../model/shared/messages";

export const apiUpdateTeam: RequestHandler = (req, res, next) => {
    const teamID = req.params.id;
    const updateTeam = req.body;
    const teamIndex = Team.findOneAndUpdate({ team_id: teamID }, updateTeam).then(data => {
        if (!data) {
            return next(APIError.errNotFound());
        }
        else {
            return next(PublicInfo.infoUpdated());
        }
    });
}
