import { RequestHandler } from "express";
import { Team } from "../../../db/team";
import { APIError, PublicInfo } from "../../../model/shared/messages";
import { BodyChecker } from "../../../model/shared/body";

export const apiRemoveAdmin: RequestHandler = async(req, res, next) =>{
    const teamID = req.params.id;
    const adminID= req.body.admins;
    const requiredFields=["admins"];

    if(!new BodyChecker(requiredFields,req.body).checkBodyFields()){
        return next(APIError.errMissingBody("Body Inexistent"));
    }
    
    Team.findOneAndUpdate({team_id: teamID}, {$pull: {admins:{ $in :adminID}}}).then(data =>{
        if (!data){
            return next(APIError.errNotFound());
        }
        
        return next(PublicInfo.infoDeleted());

    });
}
