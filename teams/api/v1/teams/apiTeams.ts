import { Router, json } from "express";
import { jsonParser } from "../general/bodyParser";


import { apiGetTeam } from "./apiGetTeam";
import { apiCreateTeam } from "./apiCreateTeam";
import { apiGetTeamID } from "./apiGetTeamID";
import { apiDeleteTeam } from "./apiDeleteTeam";
import { apiDeleteTeamsBySport } from "./apiDeleteTeamsBySport";
import { apiUpdateTeam } from "./apiUpdateTeam";
import { apiAddTeammate } from "./teammateLogic/apiAddTeammate";
import { apiReplaceTeam } from "./apiReplaceTeam";
import { apiRemoveUserTeam } from "./teammateLogic/apiRemoveUserFromTeam";
import { apiAddAdmin } from "./apiAddAdmin";
import { apiRemoveAdmin } from "./apiRemoveAdmin";
import { apiGetAllUsersFromTeam } from "./teammateLogic/getAllUsersFromTeams";
import { checkTeamFilters } from "./checkTeamFilters";


export let teamsRoute = Router();

teamsRoute.route("/")
    .get(checkTeamFilters,apiGetTeam)
    .post(jsonParser, apiCreateTeam)
    .delete(apiDeleteTeamsBySport);


teamsRoute.route("/:id")
    .get(apiGetTeamID)
    .delete(apiDeleteTeam)
    .patch(jsonParser, apiUpdateTeam)
    .put(jsonParser, apiReplaceTeam);

//teamsRoute.route("/:id/img")
    //.post(apiAddImg);

teamsRoute.route("/:id/user")
    .get(apiGetAllUsersFromTeam);
    
teamsRoute.route("/:id/user/:user")
    .patch(jsonParser, apiAddTeammate)
    .delete(jsonParser, apiRemoveUserTeam);

teamsRoute.route("/:id/admin")
    .patch(jsonParser,apiAddAdmin)
    .delete(jsonParser,apiRemoveAdmin);



