import { RequestHandler } from "express";

import { Team } from "../../../db/team";
import { TeamProfile } from "../../../model/shared/teams";

export const apiDeleteTeamsBySport: RequestHandler = (req, res, next) => {
    //Put teamSport to remove all teams that have same sport
    const teamSport = "";
    Team.deleteMany({ sport: teamSport }).then(data => {
        if (!data) {
            res.json({ "status": "error", "message": "Sport fot found" });
        }
        else {
            res.json({ "status": "success", "message": "Teams removed that sport has:"+teamSport });
        }
    });
}