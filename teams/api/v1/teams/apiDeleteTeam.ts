import { RequestHandler } from "express";
import { UserServices } from "../general/userServices";
import { Team } from "../../../db/team";
import { APIError, PublicInfo } from "../../../model/shared/messages";


export const apiDeleteTeam: RequestHandler = (req, res, next) => {
    const teamID = req.params.id;

    const teamIndex = Team.findOneAndDelete({ team_id: teamID }).then(data => {
        if (!data) {
            return next(APIError.errNotFound("Team Not Found!"));
        }
        //Team exists
        else {
            //get the array of users
            const userList = data?.toObject().users_id;
            let svc = new UserServices();
            //check if the team has users
            if (userList.length != 0) {
                //iterate through every team member and remove this team from their team
                for (const user of userList) {
                    svc.rmvTeamFromUser(teamID, user, (statusCode: number) => {
                        console.log(statusCode);
                        if (statusCode != 204) {
                            console.log('entras aqui?')
                            return next(APIError.errNotFound("Not Found!"));
                        }
                    });
                }
            }
            return next(PublicInfo.infoDeleted());
        }
    });
}
