import { RequestHandler } from "express";

import { Team } from "../../../db/team";
import { TeamProfile } from "../../../model/shared/teams";
import { PublicInfo, APIError } from "../../../model/shared/messages";

export const apiGetTeamID: RequestHandler = (req, res, next) => {
    const teamID = req.params.id;
    const teamIndex = Team.findOne({ team_id: teamID }).then(data => {
        if (!data) {
            return next(APIError.errNotFound('Team Not Found!'));
        }
        else {
            res.json(data);
        }
    });
}
