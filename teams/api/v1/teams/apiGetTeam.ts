import { RequestHandler } from "express";
import { Team } from "../../../db/team";
import { TeamProfile } from "../../../model/shared/teams";
import { TeamFilters } from "../../../model/shared/teamFilters";

//Returns all teams Paginated
export const apiGetTeam: RequestHandler = (req, res, next) => {
    const filters = new TeamFilters(req.query);

    Team.find().skip((filters.page - 1) * filters.pagination).limit(filters.pagination).then(data => {
        res.json(data.map((item: any) => new TeamProfile(item)));
    });
}