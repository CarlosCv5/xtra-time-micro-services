# Teams MicroService 

Teams microservice is one of [the multiple microservices and rest APIs along this project](https://bitbucket.org/CarlosCv5/xtra-time-micro-services/src/master/) that allow proper team methods handling.

## Install
To run this service individually you need to have Docker and Docker-Compose installed.
    1.Docker ---> https://docs.docker.com/engine/install/ubuntu/
    2.Docker-Compose --->https://docs.docker.com/compose/install/

## Run the service
    1. docker-compose build
    2. docker-compose up

# REST API

The REST API example is described below.

The host domain depends if you are running the service on the Kubernetes Cluster or individually with docker-compose.
For the two environments we have the following domains:

    Running in a Kubernetes Cluster
        1-{teamsHost} = http://doitasapro.com

    Running with docker-compose
        2-{teamsHost} = http://localhost:4060
        3- Communication between services are set to be between Pods and ClusterIP so any request communicating with other services won't work.

## Get Teams

### Request

`GET {teamsHost}/v1/teams/`

### Pagination
| query params                    | description                       |
|:----------------------------|:----------------------------------|
| `page`      | Page number (default:`1`) |
| `pagination`| Number of users to list per page (default:`100`) |

### Sucess Response
    HTTP/1.1 200 OK
```json
[
    {
        "team_id": "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        "name": "Benfica",
        "sport": "",
        "users_id": [
            "fe4b4ef8-5776-4f71-821b-292a541dea14",
            "85268232-dd82-46da-96ce-5e06f8b4c3e8"
        ],
        "admins": ["fe4b4ef8-5776-4f71-821b-292a541dea14"]
    }
    ...
]
```

### Error Response
```json
{
    "status": 400,
    "properties": "Invalid Query Parameters"
}
```
## Create a Team

### Request 

`POST {teamsHost}/v1/teams`

Body
```json
    {
        "team_id": "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        "name": "Benfica",
        "sport": "",
        "users_id": [
            "fe4b4ef8-5776-4f71-821b-292a541dea14",
        ],
        "admins": ["fe4b4ef8-5776-4f71-821b-292a541dea14"]
    }
```
### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
- Body must not be empty.
- Body must contain and `name`
```json
{
    "status": 400,
    "properties": "Body Inexistent"
}
```
## Get a Team

### Request

`GET {teamsHost}/v1/teams/:id`

### Response
    HTTP/1.1 200 OK
```json
{
    "team_id": "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
    "name": "Benfica",
    "sport": "",
    "users_id": [
        "fe4b4ef8-5776-4f71-821b-292a541dea14",
    ],
    "admins": ["fe4b4ef8-5776-4f71-821b-292a541dea14"]
}
```

### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found!"
}
```


## Delete Team

`DELETE {teamsHost}/v1/teams/:id`

When a team is deleted this service sends a request to users ClusterIP in order to remove this team from each user that belongs to this team.

### Sucess Response
```json
{
    "status": 204
}
```

### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```

## Replace Team

`PUT {teamsHost}/v1/teams/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```

## Update Team

`PATCH {teamsHost}/v1/teams/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```
## Add Admin

`PATCH {teamsHost}/v1/teams/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```

- Body must not be empty.
- Body must contain and `name`
```json
{
    "status": 400,
    "properties": "Body Inexistent"
}
```
## Remove Admin

`DELETE {teamsHost}/v1/teams/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```

- Body must not be empty.
- Body must contain and `name`
```json
{
    "status": 400,
    "properties": "Body Inexistent"
}
```

## Add User To Team

`PATCH {teamsHost}/v1/teams/:id/user/:user`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Not Found !"
}
```
## Remove User from Team

`DELETE {teamsHost}/v1/teams/:id/user/:user`

### Sucess Response
```json
{
    "status": 204
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Not Found !"
}
```

## Get All Users From a Team
`GET {teamsHost}/v1/teams/:id/user`

### Sucess Response
        [
            "fe4b4ef8-5776-4f71-821b-292a541dea14",
            "85268232-dd82-46da-96ce-5e06f8b4c3e8"
        ]
### Error Response
```json
{
    "status": 404,
    "properties": "Team not Found"
}
```

