import moongose from "mongoose";


const teamSchema = new moongose.Schema({
    team_id: String,
    name: String,
    sport: String,
    users_id: [String],
    admins:[String],
}, {
    versionKey: false
});


export const Team = moongose.model("Team", teamSchema);