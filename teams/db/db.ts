import mongoose from "mongoose";

export const connectMongo = async () => {
    try {
        await mongoose.connect("mongodb+srv://users_admin:Benfika55@cluster0-vhmet.mongodb.net/test?retryWrites=true&w=majority", {
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        });
        console.log("MongoDB Conected");
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};