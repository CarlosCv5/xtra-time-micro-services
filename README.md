# Xtra-Time Backend MicroServices Implementation
The goal is to implement a server-side application that has an environment with users, teams and games interacting with each other.


## MicroServices why?
One of the reasons I decided to go with a MicroServices architecture is that in this case it just seems natural and easy to split the business logic into MicroServices. The advantages of having this separation are huge. We can deploy and update each service independently, it's easier to debug and add features when compared to a monolithic architecture. The ability to scale individual services instead of a whole will make your life easier.


## The current implementation
This is the initial version of this application. I started to implement a Kubernetes Cluster, currently, we have 3 Pods, only Users and teams are communicating with each other so far.
Each MongoDB is Cloud Hosted.
![picture](Xtra-TimeKubernetesArchitecture.png)

## Setting up and Installation (Ubuntu)
    1.Kubectl --->https://kubernetes.io/docs/tasks/tools/install-kubectl/
    2.Minikube ---> https://kubernetes.io/docs/tasks/tools/install-minikube/
    3.Enable Ingress in the terminal run $ minikube addons enable ingress
    4.Copy the Minikube Ip with $ minikube ip
    5.To configure the host to doitasapro.com 
        5.1 $ code /etc/hosts
        5.2 paste the minikube IP "XXX.XX.X.X doitasapro.com"
    6.Skaffold ---> https://skaffold.dev/docs/install/

## Run the application
    Make sure you have minikube running
    - $ minikube status
    - $ minikube start
    - $ skaffold dev



This won't be an open-source project so this version is from 27th May 2020.