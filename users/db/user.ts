import moongose from "mongoose";


const userSchema = new moongose.Schema({
    user_id:String,
    name: String,
    sport:String,
    email:String,
    teams:[String],
    invites_id:[String]
    
}, {
    versionKey: false // You should be aware of the outcome after set to false
});




export const User = moongose.model("User", userSchema);