import moongose from "mongoose";


const TeaminvitesSchema = new moongose.Schema({
    invites_id:String,
    type: String,
    users_id:[String],
    team:String,
});


export const Invites = moongose.model("Invites", TeaminvitesSchema);

