# Users MicroService 

Users microservice is one of [the multiple microservices and rest APIs along this project](https://bitbucket.org/CarlosCv5/xtra-time-micro-services/src/master/) that allow to search and reuse our members' metadata in sophisticated ways.

## Install
To run this service individually you need to have Docker and Docker-Compose installed.
    1.Docker ---> https://docs.docker.com/engine/install/ubuntu/
    2.Docker-Compose --->https://docs.docker.com/compose/install/

## Run the service
    1. docker-compose build
    2. docker-compose up

# REST API

The REST API example is described below.

The host domain depends if you are running the service on the Kubernetes Cluster or individually with docker-compose.
For the two environments we have the following domains:

    Running in a Kubernetes Cluster
        1-{usersHost} = http://doitasapro.com

    Running with docker-compose
        2-{usersHost} = http://localhost:4050
        3- Communication between services are set to be between Pods and ClusterIP so any request communicating with other services won't work.

## Get Users

### Request

`GET {usersHost}/v1/users/`

### Pagination
| query params                    | description                       |
|:----------------------------|:----------------------------------|
| `page`      | Page number (default:`1`) |
| `pagination`| Number of users to list per page (default:`100`) |

### Sucess Response
    HTTP/1.1 200 OK
```json
[
    {
        "user_id": "85268232-dd82-46da-96ce-5e06f8b4c3e8",
        "name": "FooUser",
        "sport": "",
        "email": "foo@gmail.com",
        "teams": [
            "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        ],
        "invites_id": [
        ]
    },
    ...
]
```

### Error Response
```json
{
    "status": 400,
    "properties": "Invalid Query Parameters"
}
```
## Create a User

### Request 

`POST {usersHost}/v1/users`

Body
```json
    {
        "user_id": "85268232-dd82-46da-96ce-5e06f8b4c3e8",
        "name": "FooUser",
        "sport": "",
        "email": "foo@gmail.com",
    }
```
### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
- Body must not be empty.
- Body must contain `user_id` and `name`
```json
{
    "status": 400,
    "properties": "Body Inexistent"
}
```
## Get a User

### Request

`GET {usersHost}/v1/users/:id`

### Response
    HTTP/1.1 200 OK
```json
{
    "status": 200,
    "properties": {
        "Data": {
            "teams": [
                "5",
                "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
                "5bcf4323-6bd3-44be-bf4d-58f5c876c59f"
            ],
            "invites_id": [
                "9"
            ],
            "_id": "5eb7293bb0b2041c54b0107b",
            "user_id": "85268232-dd82-46da-96ce-5e06f8b4c3e8",
            "name": "FooUser",
            "sport": "",
            "email": "foo@gmail.com"
        }
    }
}
```

### Error Response
```json
{
    "status": 404,
    "properties": "User not Found!"
}
```


## Delete User

`DELETE {usersHost}/v1/users/:id`

### Sucess Response
```json
{
    "status": 204
}
```

### Error Response
```json
{
    "status": 404,
    "properties": "User not Found"
}
```

## Replace User

`PUT {usersHost}/v1/users/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "User not Found"
}
```

## Update User

`PATCH {usersHost}/v1/users/:id`

### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "User not Found"
}
```

## Delete Team from User

`DELETE {usersHost}/v1/users/:id/team/:team`

### Sucess Response
```json
{
    "status": 204
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Not Found !"
}
```
## Get All Teams from User

`GET {usersHost}/v1/users/:id/team`

### Sucess Response
    [
        "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        "cec4f378-d9da-4464-9bc2-68e658c9b6b5"
    ]
### Error Response
```json
{
    "status": 404,
    "properties": "Not Found !"
}
```

## Get All Invites From a User
`GET {usersHost}/v1/users/:id/invites/teaminvites`

### Sucess Response
    [
        "5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        "cec4f378-d9da-4464-9bc2-68e658c9b6b5"
    ]
### Error Response
```json
{
    "status": 404,
    "properties": "User not Found"
}
```

## Accept Invite
`PATCH {usersHost}/v1/users//:id/invites/teaminvites/:invite_id`

### Sucess Reponse
```json
{
    "status": 201
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Resource not found !"
}
```

## Unvite/Decline Invite
`DELETE {usersHost}/v1/users//:id/invites/teaminvites/:invite_id`

### Sucess Reponse
```json
{
    "status": 204
}
```
### Error Response
```json
{
    "status": 404,
    "properties": "Resource not found !"
}
```

## Create a Team Invite
`POST {usersHost}/v1/invites/teaminvites/`
```json
{
"invites_id":"cec4f378-d9da-4464-9bc2-68e658c9b6b5",
"type":"team"
,"users_id":["95f98d92-10f8-4823-8ffe-82dd7703a480","fe4b4ef8-5776-4f71-821b-292a541dea14"]
}
```

### Sucess Response
```json
{
    "status": 201
}
```

## Get All Users Invited from a Team
`GET {usersHost}/v1/invites/teaminvites/:invite_id`

### Sucess Response
    [
        "95f98d92-10f8-4823-8ffe-82dd7703a480"
    ]
### Error Response
```json
{
    "status": 404,
    "properties": "Invite not Found"
}
```