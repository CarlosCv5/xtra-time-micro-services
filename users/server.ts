import express from "express";
const app = express();
const port = 8093;
require('dotenv').config();


const cors = require('cors');
import { Router } from "express";
import { routerV1 } from "./api/v1/v1";
import { connectMongo } from "./db/db";
connectMongo();
app.use(cors());
app.use("/v1", routerV1);

app.listen(process.env.PORT || port, () => console.log("Server Started on port " + port));