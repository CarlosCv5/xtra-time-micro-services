export class UserFilters {
    readonly pagination: any
    readonly page: any
    constructor(data: any) {
        //Default pagination number is 100
        this.pagination = data.pagination
            ? parseInt(data.pagination) : 100;
        //Default page number is 1
        this.page = data.page
            ? parseInt(data.page) : 1;
    }
}