export class APIError {
    constructor(name: string,
                message: string,
                public status: number,
                public properties?: any,
                public internalProperties?: any){}
    static errNotFound(properties?: any, internalProperties?: any) {
        return new APIError("Resource not found", "The specified Resource does not exist", 404, properties, internalProperties);
    }
    static errInvalidQueryParameter(properties?: any, internalProperties?: any) {
        return new APIError("Invalid Query Parameter", "One of the query parameters specified is invalid", 400, properties, internalProperties);
    }
    static errMissingBody(properties?: any, internalProperties?: any) {
        return new APIError("Missing Body", "Missing Data in Request Body.", 400, properties, internalProperties);
    }
    static errServerError(properties?: any, internalProperties?: any) {
        return new APIError("Internal Server Error", "Request could not be carried out.", 500, properties, internalProperties);
    }
}



export class PublicInfo {
    constructor(public message: string,
                public status: number,
                public properties?: any) {};
    static infoDeleted(properties?: any) {
        return new PublicInfo("Resource Deleted", 204, properties);
    }
    static infoSucess(properties?: any) {
        return new PublicInfo("Operation sucessful", 200, properties);
    }
    static infoCreated(properties?: any) {
        return new PublicInfo("User Created", 201, properties);
    }
    static infoUpdated(properties?: any) {
        return new PublicInfo("User Updated", 201, properties);
    }
    static infoReplaced(properties?: any) {
        return new PublicInfo("User Replaced", 201, properties);
    }
}