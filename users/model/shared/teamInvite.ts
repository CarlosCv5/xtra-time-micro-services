export class TeamInvitesProfile {
    invites_id: String
    type: string
    users_id: [string]
    team: string
    constructor(data: any) {
        this.invites_id = data.invites_id;
        this.type = data.type;
        this.users_id = data.users_id;
        this.team=data.team;
    }
}