export class UserProfile {
    user_id:string
    name: string
    sport: string
    email: string
    teams:[number]
    invites_id:[number]
    constructor(data: any) {
        this.user_id=data.user_id;
        this.name = data.name;
        this.sport = data.sport;
        this.email = data.email;
        this.teams = data.teams;
        this.invites_id=data.invites_id;
        
    }
}