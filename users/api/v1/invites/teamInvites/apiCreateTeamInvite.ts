import {RequestHandler} from "express";
import { Invites } from "../../../../db/invites";
import { APIError, PublicInfo } from "../../../../model/shared/messages";


export const apiCreateTeamInvite : RequestHandler = (req, res, next) => {
    const userList=req.body.users_id;
    const newTeamInvite = {
        invites_id: req.body.invites_id || "",
        type: req.body.type || "",
        users_id: req.body.users_id || "",
        team: req.body.team || ""
    }
    next();
    new Invites(newTeamInvite).save(() => {
        return next(PublicInfo.infoCreated());
    });
};