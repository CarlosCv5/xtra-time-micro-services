import { RequestHandler } from "express";

import {Invites} from "../../../../db/invites";
import {APIError} from "../../../../model/shared/messages";

export const getAllUsersInvitedFromTeam: RequestHandler = (req,res,next) =>{
    const inviteID=req.params.invite_id;

    Invites.findOne({invites_id:inviteID},"users_id").then(data=>{
        if(!data){
            return next(APIError.errNotFound("Invite not Found"));
        }
        return res.json(data.get('users_id'));
    })
}