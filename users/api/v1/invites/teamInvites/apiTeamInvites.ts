import { Router, json } from "express";
import { jsonParser } from "../../general/bodyParser";


import { apiCreateTeamInvite } from "./apiCreateTeamInvite";
import { apiAddInvite } from "./apiAddInvites";
import { getAllUsersInvitedFromTeam } from "./getAllUsersInvitedFromTeam";



export let teamInvitesRoute = Router();

teamInvitesRoute.route("/")
    .post(jsonParser, apiCreateTeamInvite,apiAddInvite);

teamInvitesRoute.route("/:invite_id")
    .get(getAllUsersInvitedFromTeam);
    