import { RequestHandler } from "express";
import { User } from "../../../../db/user";
import { APIError, PublicInfo } from "../../../../model/shared/messages";


export const apiAddInvite: RequestHandler = async (req, res, next) => {
    const newInvite = req.body.invites_id;
    const userList = req.body.users_id;
    const option = { new: true, useFindAndModify: false };
    for (const userID of userList) {
        User.findOneAndUpdate({ user_id: userID }, { $push: { invites_id: newInvite } }, option).lean().then(data => {
            if (!data) {
                return next(APIError.errNotFound("User not Found!"));
            }
            
        });
    }
};

