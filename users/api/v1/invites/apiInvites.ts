import { Router } from "express";
import { teamInvitesRoute} from "./teamInvites/apiTeamInvites";
import { gameInvitesRoute } from "./gameInvites/apiGameInvites";
import { apiErrorHandler } from "../general/errorHandling";

export let routerInvites = Router();

routerInvites.use("/gameinvites", gameInvitesRoute);
routerInvites.use("/teaminvites",teamInvitesRoute );
routerInvites.use(apiErrorHandler);