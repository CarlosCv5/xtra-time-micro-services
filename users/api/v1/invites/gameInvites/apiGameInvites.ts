import { Router } from "express";
import { jsonParser } from "../../general/bodyParser";
import { apiCreateGameInvite } from "./apiCreateGameInvites";
export let gameInvitesRoute = Router();

gameInvitesRoute.route("/")
    .post(jsonParser,apiCreateGameInvite);
//    .post(jsonParser, apiCreateUser);