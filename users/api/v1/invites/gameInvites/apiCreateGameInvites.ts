import {RequestHandler} from "express";
import { Invites } from "../../../../db/invites";


export const apiCreateGameInvite : RequestHandler = (req, res, next) => {
    const newGameInvite = {
        invites_id: req.body.invites_id || "",
        type: req.body.type || "",
        users_id: req.body.users_id || "",
        local: req.body.local || ""
    }
    new Invites(newGameInvite).save(() => {
        res.json("New User created ");
    });
};