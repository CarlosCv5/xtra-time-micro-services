import request from "request";
import { PublicInfo } from "../../../model/shared/messages";
export class TeamServices {
    addUserToTeam(teamID: String, userID: String, cb: (statusCode: any) => any) {
        request.patch('http://teams-srv:8097/v1/teams/' + teamID +'/user/' +userID, (error: any, response: any, body: any) => {
        });
    }
    removeUserFromTeam(teamID: String, userID: String, cb: (statusCode: any) => any) {
        request.delete('http://teams-srv :8097/v1/teams/' + teamID +'/user/' +userID, (error: any, response: any, body: any) => {
        });
    }
}