import { Router } from "express";
import { usersRoute } from "./users/apiUsers";
import { logger } from "./general/logging";
import { apiErrorHandler } from "./general/errorHandling";
import { routerInvites } from "./invites/apiInvites";
export let routerV1 = Router();

routerV1.use(logger);

routerV1.use("/users", usersRoute);

routerV1.use("/invites", routerInvites);
routerV1.use(apiErrorHandler);
