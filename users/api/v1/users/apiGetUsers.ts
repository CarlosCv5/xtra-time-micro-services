import { RequestHandler } from "express";

import { User } from "../../../db/user";
import { UserProfile } from "../../../model/shared/users";
import { UserFilters } from "../../../model/shared/userFilters";

export const apiGetUsers: RequestHandler = (req, res, next) => {
    const filters = new UserFilters(req.query);

    User.find().skip((filters.page - 1) * filters.pagination).limit(filters.pagination).lean().then(data => {
        res.json(data.map((item: any) => new UserProfile(item)));
    });
}