import { Router, json } from "express";
import { jsonParser } from "../general/bodyParser";


import { apiGetUsers } from "./apiGetUsers";
import { apiCreateUser } from "./apiCreateUser";
import { apiDeleteUser } from "./apiDeleteUser";
import { apiReplaceUser } from "./apiReplaceUser";
import { apiUpdateUser } from "./apiUpdateUser";
import { apiGetUser } from "./apiGetUser";
import { apiAddUserTeam } from "./teams/apiAddUserTeam";
import { apiGetAllTeamsFromUser } from "./teams/apiGetAllTeamsFromUser";
import {  apiGetAllInvitesFromUser } from "./invites/teamInvites/apiGetInvites";
import { apiRemoveUserTeam } from "./teams/apiRemoveUserTeam";
import { apiDeleteInvite } from "./invites/teamInvites/apiDeleteInvite";
import { checkUsersFilters} from "./checkUserFilters";
import { apiUnviteUser } from "./invites/teamInvites/apiUnviteUser";


export let usersRoute = Router();

usersRoute.route("/")
    .get(checkUsersFilters,apiGetUsers)
    .post(jsonParser, apiCreateUser);

usersRoute.route("/:id")
    .get(apiGetUser)
    .delete(apiDeleteUser)
    .put(jsonParser,apiReplaceUser)
    .patch(jsonParser,apiUpdateUser);

usersRoute.route('/:id/team')
    .get(apiGetAllTeamsFromUser);

usersRoute.route('/:id/team/:team')
    .patch(apiDeleteInvite,apiAddUserTeam)
    .delete(apiRemoveUserTeam);

usersRoute.route('/:id/invites/teaminvites')
    .get(apiGetAllInvitesFromUser)

usersRoute.route("/:id/invites/teaminvites/:invite_id")
    .patch(apiDeleteInvite,apiAddUserTeam)
    .delete(apiDeleteInvite,apiUnviteUser);








    
    

