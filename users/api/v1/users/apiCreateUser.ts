import { RequestHandler } from "express";
import { User } from "../../../db/user";
import { APIError,PublicInfo } from "../../../model/shared/messages";
import { BodyChecker } from "../../../model/shared/body";


export const apiCreateUser: RequestHandler = (req, res, next) => {
    
    const requiredFields=["user_id","name"];
    if(!new BodyChecker(requiredFields,req.body).checkBodyFields()){
        return next(APIError.errMissingBody("Body Inexistent"));
    }
    const newUser = {
        user_id: req.body.user_id || "",
        name: req.body.name || "",
        sport: req.body.sport || "",
        email: req.body.email || "",
        teams: req.body.teams,
        invites_id:req.body.invites_id
    }
    new User(newUser).save(() => {
        return next(PublicInfo.infoCreated());
    });
};

