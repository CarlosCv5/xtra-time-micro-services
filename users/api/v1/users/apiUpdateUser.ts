import { RequestHandler } from "express";

import { User } from "../../../db/user";
import { UserProfile } from "../../../model/shared/users";
import { APIError, PublicInfo } from "../../../model/shared/messages";

export const apiUpdateUser: RequestHandler = async (req, res, next) => {
    const userID = req.params.id;
    const updatedUser = req.body
    const option = { new: true,useFindAndModify:false};
    User.findOneAndUpdate({ user_id: userID }, updatedUser, option).then(data => { 
        if(!data){
            return next(APIError.errNotFound("User not Found !"));
        }
        return next(PublicInfo.infoUpdated({}));

    });
}