import { RequestHandler } from "express";

import { User } from "../../../../../db/user";
import { APIError, PublicInfo } from "../../../../../model/shared/messages";
import { json } from "body-parser";
// Returns all invites from a User
export const apiGetAllInvitesFromUser: RequestHandler = (req, res, next) => {
    const userID = req.params.id;
    User.findOne({ user_id: userID }, "invites_id").then(data => {
        if (!data) {
            return next(APIError.errNotFound("User not Found"));
        }
        return res.json(data.get('invites_id'));
    });
}