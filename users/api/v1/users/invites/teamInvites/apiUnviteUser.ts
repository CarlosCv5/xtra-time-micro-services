import { RequestHandler } from "express";

import { Invites } from "../../../../../db/invites";
import { APIError, PublicInfo } from "../../../../../model/shared/messages";
import { User } from "../../../../../db/user";

export const apiUnviteUser: RequestHandler = (req, res, next) => {
    const inviteID=req.params.invite_id;
    const userID = req.params.id;

    //Remove user from Invite Doc
    const option={new:true ,useFindAndModify:false};
    Invites.findOneAndUpdate({ invites_id: inviteID, users_id:userID },{$pull: {users_id: userID}},option).then(data => {
        if(!data){
            return next(APIError.errNotFound("Resource not found !"));
        }
        return next(PublicInfo.infoDeleted());
    });
}
