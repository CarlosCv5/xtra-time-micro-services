import { RequestHandler } from "express";

import { User } from "../../../../../db/user";
import { APIError, PublicInfo } from "../../../../../model/shared/messages";

export const apiDeleteInvite: RequestHandler = (req, res, next) => {
    const userID = req.params.id;
    const inviteID=req.params.invite_id;
    const option={new:true ,useFindAndModify:false};
    User.findOneAndUpdate({ user_id: userID, invites_id:inviteID },{$pull: { invites_id: inviteID } },option).then(data => {
        if(!data){
            return next(APIError.errNotFound("Resource not found !"));
        }
        return next();
    });

}
