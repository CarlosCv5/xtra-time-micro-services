import { RequestHandler } from "express";
import { User } from "../../../db/user";
import { APIError, PublicInfo } from "../../../model/shared/messages";

export const apiReplaceUser: RequestHandler = (req, res, next) => {
    const userID = req.params.id;
    const replaceUser = req.body;
    User.replaceOne({ user_id: userID }, replaceUser).then(data => {
        if (!data) {
            return next(APIError.errNotFound("User not Found !"));
        }
        return next(PublicInfo.infoUpdated())
    });

}