import { RequestHandler } from "express";
import { User } from "../../../../db/user";
import { APIError, PublicInfo } from "../../../../model/shared/messages";
import { UserProfile } from "../../../../model/shared/users";
import { TeamServices } from "../../general/teamServices";

export const apiRemoveUserTeam: RequestHandler = (req, res, next) => {
    const TeamRemove = req.params.team;
    const userID = req.params.id;
    //Find User and remove TeamRemove from it
    User.findOneAndUpdate({ user_id: userID, teams: TeamRemove }, { $pull: { teams: TeamRemove } }).then(data => {
        if (!data) {
            return next(APIError.errNotFound('Not Found !'));
        }
        let teamServices = new TeamServices()
        teamServices.removeUserFromTeam(TeamRemove, userID, (statusCode: number) => {
            //console.log("User " + userID + " added to team" + newTeam + " statuscode : ", statusCode);
        });
        return next(PublicInfo.infoDeleted());
    });
}
