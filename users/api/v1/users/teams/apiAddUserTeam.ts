import { RequestHandler } from "express";
import { User } from "../../../../db/user";
import { APIError, PublicInfo } from "../../../../model/shared/messages";
import { TeamServices } from "../../general/teamServices";
export const apiAddUserTeam: RequestHandler = (req, res, next) => {
    const userID = req.params.id;
    const newTeam = req.params.invite_id;
    //Finds User by ID
    User.findOne({ user_id: userID }, 'teams user_id').then(doc => {
        //If User don't exist
        if (!doc) {

            return next((APIError.errNotFound('User not found !')));
        }
        else {
            const option = { new: true, useFindAndModify: false };
            User.findOneAndUpdate({ user_id: userID }, { $push: { teams: newTeam } }, option).exec().then(data => {
                //Calls Team Service to Add user to Team
                let teamServices = new TeamServices()
                teamServices.addUserToTeam(newTeam, userID, (statusCode: number) => {
                    console.log("User " + userID + " added to team" + newTeam + " statuscode : ", statusCode);
                });
                return next(PublicInfo.infoUpdated());
            });

        }
    });
}

