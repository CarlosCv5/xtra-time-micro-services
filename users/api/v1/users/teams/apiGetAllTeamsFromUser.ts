import { RequestHandler } from "express";
import { User } from "../../../../db/user";
import { APIError, PublicInfo } from "../../../../model/shared/messages";

export const apiGetAllTeamsFromUser: RequestHandler = (req, res, next) => {
    const userID= req.params.id;
    User.findOne({user_id:userID},"teams").then(data => {
        if(!data){
            return next(APIError.errNotFound("User not Found"));
        }
        return res.json(data.get('teams'));
    });
}