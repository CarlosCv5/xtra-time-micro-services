import { RequestHandler } from "express";

import { User } from "../../../db/user";
import { APIError, PublicInfo } from "../../../model/shared/messages";

export const apiDeleteUser: RequestHandler = (req, res, next) => {
    const userID = req.params.id;
    const findUser=User.findOneAndDelete({ user_id: userID }).then(data =>{ 
        if(!data)
            return next(APIError.errNotFound("User not Found"));
        
        else
            return next(PublicInfo.infoDeleted());
        
    });

}
