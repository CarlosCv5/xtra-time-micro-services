import { RequestHandler } from "express";
import { UserFilters } from '../../../model/shared/userFilters';
import { APIError } from "../../../model/shared/messages";

export const checkUsersFilters: RequestHandler = (req, res, next) => {
    const filters = new UserFilters(req.query)
    //Checks if invalid filters were provided
    for (let filter of Object.getOwnPropertyNames(req.query)) {
        if (!filters.hasOwnProperty(filter)) {
            return next(APIError.errInvalidQueryParameter("Invalid Query Parameters"));
        }
    }
    next();
};