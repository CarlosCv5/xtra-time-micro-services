import { RequestHandler, json } from "express";

import { User } from "../../../db/user";
import { APIError, PublicInfo } from "../../../model/shared/messages";


export const apiGetUser: RequestHandler = (req, res, next) => {
    const userID= req.params.id;
    User.findOne({user_id:userID}).then(data => {   
        if(!data){
            return next(APIError.errNotFound('User not Found!'));
        }     
        return next(PublicInfo.infoSucess({Data:data}));
    });
}