# Games MicroService 

Games microservice is one of [the multiple microservices and rest APIs along this project](https://bitbucket.org/CarlosCv5/xtra-time-micro-services/src/master/) that contains proper game methods.

## Install
To run this service individually you need to have Docker and Docker-Compose installed.
    1.Docker ---> https://docs.docker.com/engine/install/ubuntu/
    2.Docker-Compose --->https://docs.docker.com/compose/install/

## Run the service
    1. docker-compose build
    2. docker-compose up

# REST API

The REST API example is described below.

The host domain depends if you are running the service on the Kubernetes Cluster or individually with docker-compose.
For the two environments we have the following domains:

    Running in a Kubernetes Cluster
        1-{gamesHost} = http://doitasapro.com

    Running with docker-compose
        2-{gamesHost} = http://localhost:4070
        3- Communication between services are set to be between Pods and ClusterIP so any request communicating with other services won't work.

## Get Games

### Request

`GET {gamesHost}/v1/games/`

### Pagination
| query params                    | description                       |
|:----------------------------|:----------------------------------|
| `page`      | Page number (default:`1`) |
| `pagination`| Number of users to list per page (default:`100`) |

### Sucess Response
    HTTP/1.1 200 OK
```json
[
    {
        "game_id": "d7eeb2ce-05e2-48a0-a67a-132ff5f4d0e8",
        "type": "5",
        "players_id": [
            ""
        ],
        "local": "Evora",
        "date": "2020-06-05T00:00:00.000Z"
    }
    ...
]
```

### Error Response
```json
{
    "status": 400,
    "properties": "Invalid Query Parameters"
}
```
## Create a Team

### Request 

`POST {gamesHost}/v1/games`

Body
```json
    {
        "type":"f",
        "players_id":[""],
        "team_id":"5bcf4323-6bd3-44be-bf4d-58f5c876c59f",
        "local":"Evora",
        "date":"2020-06-05"
    }
```
### Sucess Response
```json
{
    "status": 201
}
```
### Error Response
- Body must not be empty.
- Body must contain `date` and `type`
```json
{
    "status": 400,
    "properties": "Body Inexistent"
}
```
