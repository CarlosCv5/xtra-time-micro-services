import request from "request";
import { PublicInfo } from "../../../model/shared/messages";
export class TeamServices {
    addUserToTeam(teamID: Number, userID: String, cb: (statusCode: any) => any) {
        request.patch('http://localhost:8097/v1/teams/' + teamID +'/user/' +userID, (error: any, response: any, body: any) => {
            console.log('body', body, '     body.status ',body[0].status);
            const resp=JSON.parse(body);
            let message = new PublicInfo(resp.message,resp.status);      
            cb(message.status);
        });
    }
}