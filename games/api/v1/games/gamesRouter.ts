import { Router, json } from "express";
import { jsonParser } from "../general/bodyParser";
import { apiGetAllGames } from "./getallgames";
import { apiCreateGame } from "./createGame";
import { checkGamesFilters } from "./checkGameFilters";


export let gamesRouter = Router();

gamesRouter.route("/")
    .get(checkGamesFilters,apiGetAllGames)
    .post(jsonParser,apiCreateGame);