import { RequestHandler } from "express";
import { Game } from "../../../db/game";
import { APIError,PublicInfo } from "../../../model/shared/messages";
import { BodyChecker } from "../../../model/shared/body";
import {v4 as uuidv4} from "uuid";

export const apiCreateGame: RequestHandler = (req, res, next) => {
    
    const requiredFields=["type","date"];
    if(!new BodyChecker(requiredFields,req.body).checkBodyFields()){
        return next(APIError.errMissingBody("Body Inexistent"));
    }

    const newGame = {
        game_id: uuidv4() || "",
        type: req.body.type || "",
        invites_id: req.body.invites_id || "",
        players_id: req.body.players_id || "",
        team_id: req.body.team_id,
        local:req.body.local,
        date:req.body.date
    }
    new Game(newGame).save(() => {
        return next(PublicInfo.infoCreated());
    });
};
