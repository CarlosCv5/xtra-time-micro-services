import { RequestHandler } from "express";
import { GameFilters } from '../../../model/shared/gameFilters';
import { APIError } from "../../../model/shared/messages";

export const checkGamesFilters: RequestHandler = (req, res, next) => {
    const filters = new GameFilters(req.query)
    //Checks if invalid filters were provided
    for (let filter of Object.getOwnPropertyNames(req.query)) {
        if (!filters.hasOwnProperty(filter)) {
            return next(APIError.errInvalidQueryParameter("Invalid Query Parameters"));
        }
    }
    next();
};