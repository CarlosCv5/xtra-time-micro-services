import { RequestHandler } from "express";

import { Game } from "../../../db/game";
import { GamesSkeleton } from "../../../model/shared/game";
import { GameFilters } from "../../../model/shared/gameFilters";

export const apiGetAllGames: RequestHandler = (req, res, next) => {
    const filters = new GameFilters(req.query);


    Game.find().skip((filters.page - 1) * filters.pagination).limit(filters.pagination).lean().then(data => {
        res.json(data.map((item: any) => new GamesSkeleton(item)));
    });
}