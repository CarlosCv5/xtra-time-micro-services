import { Router } from "express";
import { gamesRouter } from "./games/gamesRouter";
import{logger} from "./general/logging";
import { apiErrorHandler } from "./general/errorHandling";
export let routerV1 = Router();

routerV1.use(logger);

routerV1.use("/games", gamesRouter);


routerV1.use(apiErrorHandler);