export class BodyChecker {
    requiredFields:string[]
    body: any
    constructor(requiredFields:string[],body:any) {
        this.requiredFields=requiredFields;
        this.body = body;
    }
    checkBodyFields(): boolean {
        const givenfields = Object.getOwnPropertyNames(this.body)
        if (!this.requiredFields.every(field => givenfields.includes(field))) {
            return false;
            };
            return true;
    
}}