export class GamesSkeleton {
    game_id:string
    type: string
    invites_id: string
    players_id: string
    team_id:[string]
    local:string
    date:Date
    constructor(data: any) {
        this.game_id=data.game_id;
        this.type = data.type;
        this.invites_id = data.invites_id;
        this.players_id = data.players_id;
        this.team_id = data.teams;
        this.local=data.local;
        this.date=data.date;
        
    }
}