import moongose from "mongoose";


const GameSchema = new moongose.Schema({
    game_id:String,
    type: String,
    invites_id:[String],
    players_id:[String],
    team_id:String,
    local:String,
    date:Date
});

export const Game = moongose.model("Games", GameSchema);

